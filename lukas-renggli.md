# Titel Markdown-file-Test
Das ist ein Text
## Kapitel
Hier kommt der <mark> wichtige Teil </mark>
### Varianten
- [ ] Variante 1
- [x] Variante 2
- [ ] Variante 3

> This is a blockquote

| Lektion       | Description | Bemerkung     |
| :---          |    :----:   |          ---: |
| 1             | Einführung  | Siehe xy      |
| 2             | Start       | And more      |

Hier ein Beispiel eines JSON-Objekts:
```json
{
  "firstName": "Max",
  "lastName": "Mustermann",
  "age": 33
}
```



<lukas.renggli@sluz.ch>
